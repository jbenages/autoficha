# Ficha Bot

## Install 
```
apt install python3
```

### Pip
```
apt install pip3-python
pip3 -r requirements.txt
```

### Apt
```
apt install python3-yaml python3-requests python3-urllib3 
```

## Configuration

Copy config file:
```
cp config.example.cnf config.cnf
```

## Execute help
```
python fichabot.py -h
```
```
python3 fichabot.py -h
fichabot.py [-h] [-s [SCHEDULE]] [-d [DATE]] [-i [ID]]
                   [-c [CALCULATE_MIN]] [-t] [-x [DATE_FINISH]] [-p]

Read yaml file with dates and issues to add into tasks manager. Author Jesús
Benages Sales

optional arguments:
  -h, --help            show this help message and exit
  -s [SCHEDULE], --schedule [SCHEDULE]
                        Schedule file in yaml format. Like hours.example.yml
  -d [DATE], --date [DATE]
                        Date to filter the schedule. Ex: "2020-01-01"
  -i [ID], --id [ID]    ID alias of request to filter schedule. Ex: "gestion"
  -c [CALCULATE_MIN], --calculate-min [CALCULATE_MIN]
                        Calculate min of project with ID alias. Ex: "gestion"
  -t, --history         History of hours dedicated between 2 dates.
  -x [DATE_FINISH], --date-finish [DATE_FINISH]
                        Finish date to calculate the schedule. Ex:
                        "2020-01-01"
  -p, --prompt          Ask user and password of platform to know JSESSION.
```

## Configure config file

```
taskalias:
        gestion: 1111
        formacion: 2222

technicianid: 333333
url: "https://example.com/WorkLogAction.do"
originUrl: "https://example.com"
```

- taskalias: Array with trasnlate of numeric ID request to string human-readable id.
- technicianid: ID of the technician.

## Schedule file struct
```
2020-01-14:
  '15:10':
    desc: test
    id: gestion
    sended: '2020-01-20 11:55:16'
  '15:20':
    desc: salida
  '15:30':
    desc: test 3
    id2: gestion
  '15:40':
    desc: test 2
    id: gestion2
  '15:50':
    desc: salida
2020-01-15:
  '15:10':
    desc: test
    id: gestion
    sended: '2020-01-20 11:55:16'
  '15:50':
    desc: salida
2020-01-16:
  '09:10':
    desc: test
    entity: task
    id: auditoriaTest
  '09:40':
    desc: salida
``` 

1. The parent element are the date in this format without quotes: "YYYY-MM-DD:"
2. The hours are needed quote and should get any kind of rows, but there are the special rows:
  - desc: Description of the task.
  - id: ID of request, if the task don't have id it is dismissed to send.
  - sended: The date when the task was send. If you need to send other time the task, you should delete this key.
  - entity: This key is to mark this task as subtask.
```

## Examples

### Extract the hours used in project.
```
python3 fichabot.py -s horas.yml -c gestion -d "2020-10-13" -x "2020-10-13"
[+] gestion 	 2020-10-13 08:35 	 'Add 35 minutes' desc:'Revision de correos y teams.'
[+] gestion 	 2020-10-13 11:50 	 'Add 20 minutes' desc:'Revision de correos y teams.'
[+] gestion 	 2020-10-13 15:00 	 'Add 10 minutes' desc:'Revision de correos y teams.'
[+] gestion 	 2020-10-13 18:00 	 'Add 5 minutes' desc:'Imputar horas.'
[+] Total Minutes: 70.000000 	 Total hours: 1.166667 	 Total journey: 0.145833
```

### Extract stats of all projects
```
python3 fichabot.py -s horas.yml -t -d "2020-10-01" -x "2020-10-02"
---- Tasks ----
[+] mejorasSoftware 	 2020-10-01 08:40 	 'Add 20 minutes' desc:'Reunión con Berto.'
[+] gestion 	 2020-10-01 09:00 	 'Add 25 minutes' desc:'Revisión de correos y teams.'
[+] Cliente1Auditoria 	 2020-10-01 09:25 	 'Add 55 minutes' desc:'Crear documento de planificación.'
[+] proyectoImasD 	 2020-10-01 10:20 	 'Add 10 minutes' desc:'Buscar correos de responsables de los dipositivos del proyecto.'
[+] proyectoImasD 	 2020-10-01 11:00 	 'Add 60 minutes' desc:'Reunión con Pepa para hablar de los entregables que necesita Pepe.'
[+] Cliente3Asistencia 	 2020-10-01 12:00 	 'Add 25 minutes' desc:'Desbloquear usuario Fernando.'
[+] proyectoImasD 	 2020-10-01 12:25 	 'Add 40 minutes' desc:'revisión de bugs reparados.'
[+] mejorasSoftware 	 2020-10-01 13:25 	 'Add 25 minutes' desc:'Buscar cuestionarios de temas 11-14 en correos.'
[+] proyectoImasD 	 2020-10-01 14:30 	 'Add 80 minutes' desc:'revisión de bugs reparados de software.'
[+] auditoriaCliente4 	 2020-10-01 15:50 	 'Add 10 minutes' desc:'Contestar a correo de Juanjo para proseguir con las auditorias.'
[+] mejorasSoftware 	 2020-10-01 16:00 	 'Add 30 minutes' desc:'Reunión con Angels, Luis y Amparo para hablar de las mejoras.'
[+] Cliente2AuditoriaInterna 	 2020-10-01 16:30 	 'Add 85 minutes' desc:'Realizar informe.'
[+] gestion 	 2020-10-01 17:55 	 'Add 5 minutes' desc:'Imputar horas.'
[+] gestion 	 2020-10-02 08:30 	 'Add 15 minutes' desc:'Revisar correos y teams.'
[+] Cliente1Auditoria 	 2020-10-02 08:45 	 'Add 45 minutes' desc:'Kickoff con el cliente.'
[+] Cliente1Auditoria 	 2020-10-02 09:30 	 'Add 30 minutes' desc:'Terminar documentación de planificación para el envío.'
[+] proyectoImasD 	 2020-10-02 10:00 	 'Add 25 minutes' desc:'Reunión con María y Luis para recopilar los requirimientos.'
[+] Cliente1Auditoria 	 2020-10-02 10:25 	 'Add 55 minutes' desc:'Terminar documentación de planificación para el envío.'
[+] Cliente1Auditoria 	 2020-10-02 11:45 	 'Add 15 minutes' desc:'Terminar documentación de planificación para el envío.'
[+] gestion 	 2020-10-02 12:00 	 'Add 60 minutes' desc:'Reunión de proyectos.'
[+] mejorasSoftware 	 2020-10-02 14:00 	 'Add 20 minutes' desc:'Reunión con Angels para el tema del nuevo curso.'
[+] auditoriaCliente4 	 2020-10-02 14:20 	 'Add 15 minutes' desc:'Redactar correo de contestación para Juanjo para acordar las fechas de auditorias.'
[+] gestion 	 2020-10-02 15:50 	 'Add 50 minutes' desc:'Revisar correos y teams. Revisar tareas de Septiembre. Revisar proyectos octubre.'
[+] gestion 	 2020-10-02 17:10 	 'Add 20 minutes' desc:'Revisar proyectos octubre.'
[+] gestion 	 2020-10-02 17:30 	 'Add 5 minutes' desc:'Imputar horas.'
---- Project ----
[+] Project: mejorasSoftware 	 Total Minutes: 95.000000 	 Total hours: 1.583333 	 Total journey: 0.197917
[+] Project: gestion 	 Total Minutes: 180.000000 	 Total hours: 3.000000 	 Total journey: 0.375000
[+] Project: Cliente1Auditoria 	 Total Minutes: 200.000000 	 Total hours: 3.333333 	 Total journey: 0.416667
[+] Project: proyectoImasD 	 Total Minutes: 215.000000 	 Total hours: 3.583333 	 Total journey: 0.447917
[+] Project: Cliente3Asistencia 	 Total Minutes: 25.000000 	 Total hours: 0.416667 	 Total journey: 0.052083
[+] Project: auditoriaCliente4 	 Total Minutes: 25.000000 	 Total hours: 0.416667 	 Total journey: 0.052083
[+] Project: Cliente2AuditoriaInterna 	 Total Minutes: 85.000000 	 Total hours: 1.416667 	 Total journey: 0.177083
---- Total ----
[+] Total sum 	 Total Minutes: 825.000000 	 Total hours: 13.750000 	 Total journey: 1.718750
```

