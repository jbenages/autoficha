import sys,argparse,os,yaml,time
import datetime
import requests
from pprint import pprint
from shutil import copyfile
from urllib3.exceptions import InsecureRequestWarning
import getpass

# Suppress only the single warning from urllib3 needed.
requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

config = yaml.safe_load(open(os.path.abspath(sys.path[0])+"/config.yml"))

sessionR = requests.Session() 

def login( user, password ):

    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Language': 'es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Origin': config["originUrl"],
        'Connection': 'keep-alive',
        'Referer': config["originUrl"],
        'Upgrade-Insecure-Requests': '1',
    }

    sessionR.get(config["originUrl"]+'/HomePage.do?logout=true&logoutSkipNV2Filter=true', headers=headers, verify=False)
    
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Language': 'es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Origin': config["originUrl"],
        'Connection': 'keep-alive',
        'Referer': config["originUrl"],
        'Upgrade-Insecure-Requests': '1',
    }
    
    data = [
      ('j_username', user),
      ('j_password', password),
      ('LDAPEnable', 'false'),
      ('hidden', 'Seleccione un dominio'),
      ('hidden', 'Para Dominio'),
      ('AdEnable', 'false'),
      ('DomainCount', '0'),
      ('LocalAuth', 'No'),
      ('LocalAuthWithDomain', 'No'),
      ('dynamicUserAddition_status', 'false'),
      ('localAuthEnable', 'true'),
      ('logonDomainName', '-1'),
      ('loginButton', 'Iniciar sesi\xF3n'),
    ]
    return sessionR.post(config["originUrl"]+'/j_security_check', headers=headers, data=data, verify=False)

def sendData( asocEntId, startTime, totalTime, endTime, description, entity):

    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:71.0) Gecko/20100101 Firefox/71.0',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Language': 'es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Origin': config["originUrl"],
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
    }

    data = {
      'taskid': 'null',
      'from': 'request',
      'chargeid': '',
      'createdtime': '',
      'createdby': '',
      'module': 'request',
      'associatedEntity': entity,
      'associatedEntityID': asocEntId,
      'scopeid': '301',
      'technicianid': config["technicianid"],
      'ts_starttime': startTime,
      'total_timespent': totalTime,
      'timespenthrs': '',
      'timespentmins': '10',
      'ts_endtime': endTime,
      'other_charge': '',
      'description': description,
      'submitaction': 'SaveAndAddNew'
    }
    
    return sessionR.post(config["url"], headers=headers, data=data, verify=False)

def processTask( dateSchedule, date, hour, lastHour, pathSchedule ):
    lastTaskDatetime = datetime.datetime.strptime(date.strftime("%Y-%m-%d")+" "+lastHour, '%Y-%m-%d %H:%M')
    lastTaskTimeStamp = int(datetime.datetime.timestamp(lastTaskDatetime)*1000)
    taskDatetime = datetime.datetime.strptime(date.strftime("%Y-%m-%d")+" "+hour, '%Y-%m-%d %H:%M')
    taskTimeStamp = int(datetime.datetime.timestamp(taskDatetime)*1000)
    diff = taskTimeStamp - lastTaskTimeStamp
    if dateSchedule[date][lastHour]["id"] in config["taskalias"]:
        idTask = config["taskalias"][dateSchedule[date][lastHour]["id"]]
    else:
        idTask = dateSchedule[date][lastHour]["id"]

    countSecs = diff

    if "sended" in dateSchedule[date][lastHour]:
        print("[-] The tasks (id: '%s', desc: '%s', date: '%s') is sended on this date: '%s'"%( dateSchedule[date][lastHour]["id"],dateSchedule[date][lastHour]["desc"], lastTaskDatetime.strftime("%Y-%m-%d %H:%M:%S"), dateSchedule[date][lastHour]["sended"] ))
    else:
        if "entity" not in dateSchedule[date][lastHour]:
            entity = "request"
        else:
            entity = dateSchedule[date][lastHour]["entity"]

        requestUrl = sendData( idTask, lastTaskTimeStamp, diff, lastTaskTimeStamp+diff, dateSchedule[date][lastHour]["desc"], entity )
        state="[-]"
        if requestUrl.status_code == 200 and 'El registro de trabajo se añadió correctamente' in requestUrl.text:
            state="[+]"
            dateSchedule[date][lastHour]["sended"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") 
            with open(pathSchedule, 'w') as file:
                yaml.dump(dateSchedule, file, default_flow_style=False, indent=True, encoding='utf-8', width=10000, allow_unicode=True)
        print( "%s %s - '%s'\t '%s'"%(state,lastTaskDatetime.strftime("%Y-%m-%d %H:%M"), dateSchedule[date][lastHour]["id"] , dateSchedule[date][lastHour]["desc"] ) )

def loopDate( dateSchedule, date, idRequest, pathSchedule ):
    lastHour = None
    for hour in dateSchedule[date]:
        if lastHour is not None:
            if 'id' in dateSchedule[date][lastHour]:
                if idRequest != None:
                    if idRequest == dateSchedule[date][lastHour]["id"]:
                        processTask( dateSchedule, date, hour, lastHour, pathSchedule )
                else:
                    processTask( dateSchedule, date, hour, lastHour, pathSchedule )

        lastHour = hour

def loopBetweenDates( dateSchedule, date, dateFinish, idRequest ):
    sumSeconds = 0
    
    dateFormat = datetime.datetime.strptime(date, '%Y-%m-%d').date()
    dateFinishFormat = datetime.datetime.strptime(dateFinish, '%Y-%m-%d').date()

    for iDate in dateSchedule:
        if dateFormat <= iDate <= dateFinishFormat:
            
            lastHour = None   
            for hour in dateSchedule[iDate]:
                if lastHour is not None:
                    if 'id' in dateSchedule[iDate][lastHour]:
                        if idRequest in dateSchedule[iDate][lastHour]["id"]:
                            lastTaskDatetime = datetime.datetime.strptime(iDate.strftime("%Y-%m-%d")+" "+lastHour, '%Y-%m-%d %H:%M')
                            lastTaskTimeStamp = int(datetime.datetime.timestamp(lastTaskDatetime)*1000)
                            taskDatetime = datetime.datetime.strptime(iDate.strftime("%Y-%m-%d")+" "+hour, '%Y-%m-%d %H:%M')
                            taskTimeStamp = int(datetime.datetime.timestamp(taskDatetime)*1000)
                            diff = taskTimeStamp - lastTaskTimeStamp
                            print( "[+] %s \t %s \t 'Add %d minutes' desc:'%s'"%(dateSchedule[iDate][lastHour]["id"],lastTaskDatetime.strftime("%Y-%m-%d %H:%M"),( diff/60000 ),dateSchedule[iDate][lastHour]["desc"] ) )
                            sumSeconds += diff

                lastHour = hour
    print( "[+] Total Minutes: %f \t Total hours: %f \t Total journey: %f"%((sumSeconds/60000 ),((sumSeconds/60000)/60),(((sumSeconds/60000)/60)/8) ))

def makeHistoryTasks( dateSchedule, date, dateFinish ):

    tasks = {}

    totalMin = 0
    
    dateFormat = datetime.datetime.strptime(date, '%Y-%m-%d').date()
    dateFinishFormat = datetime.datetime.strptime(dateFinish, '%Y-%m-%d').date()
    print("---- Tasks ----")
    for iDate in dateSchedule:
        if dateFormat <= iDate <= dateFinishFormat:
            
            lastHour = None   
            for hour in dateSchedule[iDate]:
                if lastHour is not None:
                    if 'id' in dateSchedule[iDate][lastHour]:
                        if dateSchedule[iDate][lastHour]["id"] not in tasks:
                            tasks[dateSchedule[iDate][lastHour]["id"]] = 0
                        
                        lastTaskDatetime = datetime.datetime.strptime(iDate.strftime("%Y-%m-%d")+" "+lastHour, '%Y-%m-%d %H:%M')
                        lastTaskTimeStamp = int(datetime.datetime.timestamp(lastTaskDatetime)*1000)
                        taskDatetime = datetime.datetime.strptime(iDate.strftime("%Y-%m-%d")+" "+hour, '%Y-%m-%d %H:%M')
                        taskTimeStamp = int(datetime.datetime.timestamp(taskDatetime)*1000)
                        diff = taskTimeStamp - lastTaskTimeStamp
                        print( "[+] %s \t %s \t 'Add %d minutes' desc:'%s'"%(dateSchedule[iDate][lastHour]["id"],lastTaskDatetime.strftime("%Y-%m-%d %H:%M"),( diff/60000 ),dateSchedule[iDate][lastHour]["desc"] ) )
                        tasks[dateSchedule[iDate][lastHour]["id"]] += diff

                lastHour = hour
    print("---- Project ----")
    for task in tasks:
        totalMin += tasks[task]
        print( "[+] Project: %s \t Total Minutes: %f \t Total hours: %f \t Total journey: %f"%(task,(tasks[task]/60000 ),((tasks[task]/60000)/60),(((tasks[task]/60000)/60)/8) ))
    print("---- Total ----")
    print( "[+] Total sum \t Total Minutes: %f \t Total hours: %f \t Total journey: %f"%((totalMin/60000 ),((totalMin/60000)/60),(((totalMin/60000)/60)/8) ))

def main():

    parser = argparse.ArgumentParser(description='Read yaml file with dates and issues to add into tasks manager. Author Jesús Benages Sales')
    parser.add_argument('-s','--schedule', nargs='?', help='Schedule file in yaml format. Like hours.example.yml')
    parser.add_argument('-d','--date', nargs='?', help='Date to filter the schedule. Ex: "2020-01-01"')
    parser.add_argument('-i','--id', nargs='?', help='ID alias of request to filter schedule. Ex: "gestion"')
    parser.add_argument('-c','--calculate-min', nargs='?', help='Calculate min of project with ID alias. Ex: "gestion"')
    parser.add_argument('-t','--history',dest='history', action='store_true', help='History of hours dedicated between 2 dates.')
    parser.add_argument('-x','--date-finish', nargs='?', help='Finish date to calculate the schedule. Ex: "2020-01-01"')
    parser.add_argument('-p','--prompt', dest='prompt', action='store_true', help='Ask user and password of platform to know JSESSION.')
    parser.set_defaults(prompt=False)

    args = parser.parse_args()

    if args.prompt :
        print("user:")
        user = input()
        password = getpass.getpass()
        requestLogin = login(user, password)
        if requestLogin.status_code == 200 and 'MySchedule.do' in requestLogin.text:
            print("[+] Login")
        else:
            print("[!] Login fail")
            sys.exit(0)

    if args.schedule != None:
        schedule = yaml.safe_load(open(args.schedule))

        pathSchedule = args.schedule
        copyfile(pathSchedule, pathSchedule+"-"+datetime.datetime.now().strftime("%Y%m%d%H%M%S"))

        idRequest = None
        if args.calculate_min != None:
            idRequest = args.calculate_min
            dateInit = args.date
            if args.date_finish != None:
                dateFinish = args.date_finish
            else:
                dateFinish = dateInit
            loopBetweenDates( schedule, dateInit, dateFinish, idRequest )

        elif args.history:
            if args.date != None:
                dateInit = args.date
                if args.date_finish != None:
                    dateFinish = args.date_finish
                else:
                    dateFinish = dateInit
                makeHistoryTasks(schedule, dateInit, dateFinish)
            else:
                 print("[-] History need param -d.")



        else:
            if args.id != None:
                idRequest = args.id

            if args.date != None:
                date = args.date
                date = datetime.datetime.strptime(date, '%Y-%m-%d').date()
                if date in schedule:
                    loopDate(schedule, date, idRequest, pathSchedule )
                else:
                    print("[!] Date: %s not exist in file." % (date) )
            else:
                for date in schedule:
                    loopDate(schedule, date, idRequest, pathSchedule )

    else:
        sys.exit("No schedule")

if __name__ == "__main__":
    main()
